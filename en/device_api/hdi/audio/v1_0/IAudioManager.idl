/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup Audio
 * @{
 *
 * @brief Defines audio-related APIs, including data types and functions for loading drivers, accessing a driver adapter, and rendering and capturing audios.
 *
 *
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @file IAudioManager.idl
 *
 * @brief Declares APIs for audio adapter management and loading.
 *
 * @since 3.2
 * @version 1.0
 */

/**
 * @brief Defines the package path of the audio APIs.
 *
 * @since 3.2
 * @version 1.0
 */
package ohos.hdi.audio.v1_0;

import ohos.hdi.audio.v1_0.AudioTypes;
import ohos.hdi.audio.v1_0.IAudioAdapter;

/**
 * @brief Manages audio adapters.
 *
 * An audio adapter driver is loaded based on the audio adapter descriptor delivered by the audio service.
 *
 * @see IAudioAdapter
 *
 * @since 3.2
 * @version 1.0
 */
interface IAudioManager {

    /**
     * @brief Obtains the list of all adapters supported by the audio driver.
     *
     * @param descs Indicates the list of audio adapter descriptors. For details, see {@link AudioAdapterDescriptor}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see LoadAdapter
     *
     * @since 3.2
     * @version 1.0
     */
    GetAllAdapters([out] struct AudioAdapterDescriptor[] descs);

    /**
     * @brief Loads the driver for an audio adapter.
     *
     * For example, to load a USB driver, you may need to load a dynamic-link library (*.so) in specific implementation.
     *
     * @param desc Indicates the descriptor of the audio adapter. For details, see {@link AudioAdapterDescriptor}.
     * @param adapter Indicates the audio adapter obtained. For details, see {@link IAudioAdapter}.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @see GetAllAdapters
     * @see UnloadAdapter
     *
     * @since 3.2
     * @version 1.0
     */
    LoadAdapter([in] struct AudioAdapterDescriptor desc, [out] IAudioAdapter adapter);

    /**
     * @brief Unloads the driver of an audio adapter.
     *
     * @param adapter Indicates the audio adapter whose driver will be unloaded.
     *
     * @see LoadAdapter
     *
     * @since 3.2
     * @version 1.0
     */
    UnloadAdapter([in] String adapterName);

    /**
     * @brief Releases the <b>AudioManager</b> object.
     *
     * @return Returns <b>0</b> if the operation is successful; returns a negative value otherwise.
     *
     * @since 3.2
     * @version 1.0
     */
    ReleaseAudioManagerObject();
}
/** @} */
