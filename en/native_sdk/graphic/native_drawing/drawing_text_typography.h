/*
 * Copyright (c) 2021-2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H
#define C_INCLUDE_DRAWING_TEXT_TYPOGRAPHY_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Provides the functions for 2D graphics rendering, text drawing, and image display.
 * This module does not provide the pixel unit. The pixel unit to use is consistent with the application context
 * environment. In the ArkUI development environment, the default pixel unit vp is used.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_text_typography.h
 *
 * @brief Declares the functions related to the typography in the drawing module.
 *
 * File to include: native_drawing/drawing_text_typography.h
 * @library libnative_drawing.so
 * @since 8
 * @version 1.0
 */

#include "drawing_canvas.h"
#include "drawing_color.h"
#include "drawing_font.h"
#include "drawing_text_declaration.h"
#include "drawing_types.h"

#include "stdint.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Enumerates the text directions.
 */
enum OH_Drawing_TextDirection {
    /** Right to left (RTL). */
    TEXT_DIRECTION_RTL,
    /** Left to right (LTR). */
    TEXT_DIRECTION_LTR,
};

/**
 * @brief Enumerates the text alignment modes.
 */
enum OH_Drawing_TextAlign {
    /** Left-aligned. */
    TEXT_ALIGN_LEFT,
    /** Right-aligned. */
    TEXT_ALIGN_RIGHT,
    /** Center-aligned. */
    TEXT_ALIGN_CENTER,
    /**
     * Justified, which means that each line (except the last line) is stretched so that every line has equal width,
     * and the left and right margins are straight.
     */
    TEXT_ALIGN_JUSTIFY,
    /**
     * <b>TEXT_ALIGN_START</b> achieves the same effect as <b>TEXT_ALIGN_LEFT</b>
     * when <b>OH_Drawing_TextDirection</b> is <b>TEXT_DIRECTION_LTR</b>;
     * it achieves the same effect as <b>TEXT_ALIGN_RIGHT</b>
     * when <b>OH_Drawing_TextDirection</b> is <b>TEXT_DIRECTION_RTL</b>.
     */
    TEXT_ALIGN_START,
    /**
     * <b>TEXT_ALIGN_END</b> achieves the same effect as <b>TEXT_ALIGN_RIGHT</b>
     * when <b>OH_Drawing_TextDirection</b> is <b>TEXT_DIRECTION_LTR</b>;
     * it achieves the same effect as <b>TEXT_ALIGN_LEFT</b>
     * when <b>OH_Drawing_TextDirection</b> is <b>TEXT_DIRECTION_RTL</b>.
     */
    TEXT_ALIGN_END,
};

/**
 * @brief Enumerates the font weights.
 */
enum OH_Drawing_FontWeight {
    /** Thin. */
    FONT_WEIGHT_100,
    /** Extra-light. */
    FONT_WEIGHT_200,
    /** Light. */
    FONT_WEIGHT_300,
    /** Normal/Regular. */
    FONT_WEIGHT_400,
    /** Medium. */
    FONT_WEIGHT_500,
    /** Semi-bold. */
    FONT_WEIGHT_600,
    /** Bold. */
    FONT_WEIGHT_700,
    /** Extra-bold. */
    FONT_WEIGHT_800,
    /** Black. */
    FONT_WEIGHT_900,
};

/**
 * @brief Enumerates the text baselines.
 */
enum OH_Drawing_TextBaseline {
    /** Alphabetic, where the letters in alphabets like English sit on. */
    TEXT_BASELINE_ALPHABETIC,
    /** Ideographic. The baseline is at the bottom of the text area. */
    TEXT_BASELINE_IDEOGRAPHIC,
};

/**
 * @brief Enumerates the text decorations.
 */
enum OH_Drawing_TextDecoration {
    /** No decoration. */
    TEXT_DECORATION_NONE = 0x0,
    /** A underline is used for decoration. */
    TEXT_DECORATION_UNDERLINE = 0x1,
    /** An overline is used for decoration. */
    TEXT_DECORATION_OVERLINE = 0x2,
    /** A strikethrough is used for decoration. */
    TEXT_DECORATION_LINE_THROUGH = 0x4,
};

/**
 * @brief Enumerates the font styles.
 */
enum OH_Drawing_FontStyle {
    /** Normal style. */
    FONT_STYLE_NORMAL,
    /** Italic. */
    FONT_STYLE_ITALIC,
};

/**
 * @brief Enumerates the vertical alignment modes of placeholders.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_PlaceholderVerticalAlignment {
    /** Aligned to the baseline. */
    ALIGNMENT_OFFSET_AT_BASELINE,
    /** Aligned above the baseline. */
    ALIGNMENT_ABOVE_BASELINE,
    /** Aligned below the baseline. */
    ALIGNMENT_BELOW_BASELINE,
    /** Aligned to the top of the row box. */
    ALIGNMENT_TOP_OF_ROW_BOX,
    /** Aligned to the bottom of the row box. */
    ALIGNMENT_BOTTOM_OF_ROW_BOX,
    /** Aligned to the center of the row box. */
    ALIGNMENT_CENTER_OF_ROW_BOX,
} OH_Drawing_PlaceholderVerticalAlignment;

/**
 * @brief Describes a placeholder that acts as a span.
 *
 * @since 11
 * @version 1.0
 */
typedef struct OH_Drawing_PlaceholderSpan {
    /** Width of the placeholder. */
    double width;
    /** Height of the placeholder. */
    double height;
    /** Alignment mode of the placeholder. */
    OH_Drawing_PlaceholderVerticalAlignment alignment;
    /** Baseline of the placeholder. */
    OH_Drawing_TextBaseline baseline;
    /** Baseline offset of the placeholder. */
    double baselineOffset;
} OH_Drawing_PlaceholderSpan;

/**
 * @brief Enumerates the text decoration styles.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_TextDecorationStyle {
    /** Solid style. */
    TEXT_DECORATION_STYLE_SOLID,
    /** Double style. */
    TEXT_DECORATION_STYLE_DOUBLE,
    /** Dotted style. */
    TEXT_DECORATION_STYLE_DOTTED,
    /** Dashed style. */
    TEXT_DECORATION_STYLE_DASHED,
    /** Wavy style. */
    TEXT_DECORATION_STYLE_WAVY,
} OH_Drawing_TextDecorationStyle;

/**
 * @brief Enumerates the ellipsis styles.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_EllipsisModal {
    /** Places the ellipsis in the text header. */
    ELLIPSIS_MODAL_HEAD = 0,
    /** Places the ellipsis in the middle of the text. */
    ELLIPSIS_MODAL_MIDDLE = 1,
    /** Places the ellipsis at the end of the text. */
    ELLIPSIS_MODAL_TAIL = 2,
} OH_Drawing_EllipsisModal;

/**
 * @brief Enumerates the text break strategies.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_BreakStrategy {
    /** Each line is filled as much as possible during line break. */
    BREAK_STRATEGY_GREEDY = 0,
    /** Text continuity is preferentially considered during line break. */
    BREAK_STRATEGY_HIGH_QUALITY = 1,
    /** Line breaks are performed at the word boundary. */
    BREAK_STRATEGY_BALANCED = 2,
} OH_Drawing_BreakStrategy;

/**
 * @brief Enumerates the word break types.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_WordBreakType {
    /** Normal mode. */
    WORD_BREAK_TYPE_NORMAL = 0,
    /** Breaks the words at any character to prevent overflow. */
    WORD_BREAK_TYPE_BREAK_ALL = 1,
    /** Breaks the words at arbitrary points to prevent overflow. */
    WORD_BREAK_TYPE_BREAK_WORD = 2,
} OH_Drawing_WordBreakType;

/**
 * @brief Enumerates the rectangle height styles.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_RectHeightStyle {
    /** Tight style. */
    RECT_HEIGHT_STYLE_TIGHT,
    /** Maximum style. */
    RECT_HEIGHT_STYLE_MAX,
    /** Middle style that includes the line spacing. */
    RECT_HEIGHT_STYLE_INCLUDELINESPACEMIDDLE,
    /** Top style that includes the line spacing. */
    RECT_HEIGHT_STYLE_INCLUDELINESPACETOP,
    /** Bottom style that includes the line spacing. */
    RECT_HEIGHT_STYLE_INCLUDELINESPACEBOTTOM,
    /** Structure style. */
    RECT_HEIGHT_STYLE_STRUCT,
} OH_Drawing_RectHeightStyle;

/**
 * @brief Enumerates the rectangle width styles.
 *
 * @since 11
 * @version 1.0
 */
typedef enum OH_Drawing_RectWidthStyle {
    /** Tight style. */
    RECT_WIDTH_STYLE_TIGHT,
    */Maximum style. */
    RECT_WIDTH_STYLE_MAX,
} OH_Drawing_RectWidthStyle;

/**
 * @brief Defines a struct that describes the detailed information about a system font.
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_FontDescriptor {
    /** File path of the system font. */
    char* path;
    /** PostScript name that uniquely identifies the system font. */
    char* postScriptName;
    /** Full name of the system font. */
    char* fullName;
    /** Family of the system font. */
    char* fontFamily;
    /** Subfamily of the system font. */
    char* fontSubfamily;
    /** Weight of the system font. */
    int weight;
    /** Width of the system font. */
    int width;
    /** Slope of the system font. */
    int italic;
    /**
     * Whether the system font is monospaced.
     * The value <b>true</b> means that the system font is monospaced, and <b>false</b> means the opposite.
     */
    bool monoSpace;
    /**
     * Whether the system font supports symbols.
     * The value <b>true</b> means that the system font supports symbols, and <b>false</b> means the opposite.
     */
    bool symbolic;
} OH_Drawing_FontDescriptor;

/**
 * @brief Defines a struct that describes the measurement information about a line of text.
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_LineMetrics {
    /** Part of a lowercase letter that extends beyond the meanline. */
    double ascender;
    /** Part of a lowercase letter that extends below the baseline. */
    double descender;
    /** Height of an uppercase letter above the baseline. */
    double capHeight;
    /** Height of a lowercase letter, specifically the lowercase x, not including ascenders and descenders. */
    double xHeight;
    /** Horizontal space taken up by a character. */
    double width;
    /** Line height. */
    double height;
    /**
     * Distance from the left edge of the leftmost character to the left edge of the container.
     * For left alignment, the value is 0. For right alignment, the value is the container width minus the text width.
     */
    double x;
    /**
     * Height from the top edge of the character to the top of the container.
     * The first line is 0, and the second line is the height of the first line.
     */
    double y;
    /** Index of the first character in the line. */
    size_t startIndex;
    /** Index of the last character in the line. */
    size_t endIndex;
    /** Measurement information of the first character. */
    OH_Drawing_Font_Metrics firstCharMetrics;
} OH_Drawing_LineMetrics;

/**
 * @brief Creates an <b>OH_Drawing_TypographyStyle</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_TypographyStyle</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyStyle* OH_Drawing_CreateTypographyStyle(void);

/**
 * @brief Destroys an <b>OH_Drawing_TypographyStyle</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyStyle(OH_Drawing_TypographyStyle*);

/**
 * @brief Sets the text direction.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Text direction. For details about the available options, see {@link OH_Drawing_TextDirection}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextDirection(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextDirection */);

/**
 * @brief Sets the text alignment mode.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Text alignment mode. For details about the available options, see {@link OH_Drawing_TextAlign}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextAlign(OH_Drawing_TypographyStyle*, int /* OH_Drawing_TextAlign */);

/**
 * @brief Obtains the text alignment mode.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @return Returns the text alignment mode.
 * @since 12
 * @version 1.0
 */
int OH_Drawing_TypographyGetEffectiveAlignment(OH_Drawing_TypographyStyle* style);

/**
 * @brief Sets the maximum number of lines in the text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Maximum number of lines.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextMaxLines(OH_Drawing_TypographyStyle*, int /* maxLines */);

/**
 * @brief Creates an <b>OH_Drawing_TextStyle</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_TextStyle</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_TextStyle* OH_Drawing_CreateTextStyle(void);

/**
 * @brief Obtains the text style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @return Returns the pointer to the {@link OH_Drawing_TextStyle} object.
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextStyle* OH_Drawing_TypographyGetTextStyle(OH_Drawing_TypographyStyle* style);

/**
 * @brief Destroys an <b>OH_Drawing_TextStyle</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTextStyle(OH_Drawing_TextStyle*);

/**
 * @brief Sets the text color.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param uint32_t Color.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief Sets the font size.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Font size.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontSize(OH_Drawing_TextStyle*, double /* fontSize */);

/**
 * @brief Sets the font weight.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Font weight.
 * For details about the available options, see {@link OH_Drawing_FontWeight}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontWeight(OH_Drawing_TextStyle*, int /* OH_Drawing_FontWeight */);

/**
 * @brief Sets the text baseline.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Text baseline. For details about the available options, see {@link OH_Drawing_TextBaseline}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBaseLine(OH_Drawing_TextStyle*, int /* OH_Drawing_TextBaseline */);

/**
 * @brief Sets the text decoration.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Text decoration. For details about the available options, see {@link OH_Drawing_TextDecoration}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecoration(OH_Drawing_TextStyle*, int /* OH_Drawing_TextDecoration */);

/**
 * @brief Sets the color for the text decoration.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param uint32_t Color.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationColor(OH_Drawing_TextStyle*, uint32_t /* color */);

/**
 * @brief Sets the font height.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Font height.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontHeight(OH_Drawing_TextStyle*, double /* fontHeight */);

/**
 * @brief Sets the font families.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Number of font families.
 * @param char Pointer to the font families.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontFamilies(OH_Drawing_TextStyle*,
    int /* fontFamiliesNumber */, const char* fontFamilies[]);

/**
 * @brief Sets the font style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Font style. For details about the available options, see {@link OH_Drawing_FontStyle}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleFontStyle(OH_Drawing_TextStyle*, int /* OH_Drawing_FontStyle */);

/**
 * @brief Sets the locale.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param char Pointer to the locale.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_SetTextStyleLocale(OH_Drawing_TextStyle*, const char*);

/**
 * @brief Sets the foreground brush.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_Brush Pointer to an {@link OH_Drawing_Brush} object, which is obtained by calling
 * {@link OH_Drawing_BrushCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleForegroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief Obtains the foreground brush.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_Brush Pointer to an {@link OH_Drawing_Brush} object, which is obtained by calling
 * {@link OH_Drawing_BrushCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleGetForegroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief Sets the foreground pen.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object, which is obtained by calling
 * {@link OH_Drawing_PenCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleForegroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief Obtains the foreground pen.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object, which is obtained by calling
 * {@link OH_Drawing_PenCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleGetForegroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief Sets the background brush.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_Brush Pointer to an {@link OH_Drawing_Brush} object, which is obtained by calling
 * {@link OH_Drawing_BrushCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBackgroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief Obtains the background brush.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_Brush Pointer to an {@link OH_Drawing_Brush} object, which is obtained by calling
 * {@link OH_Drawing_BrushCreate}.
 * @since 12
 * @version 1.0
 */
 void OH_Drawing_TextStyleGetBackgroundBrush(OH_Drawing_TextStyle*, OH_Drawing_Brush*);

/**
 * @brief Sets the background pen.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object, which is obtained by calling
 * {@link OH_Drawing_PenCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTextStyleBackgroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief Obtains the background pen.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_Pen Pointer to an {@link OH_Drawing_Pen} object, which is obtained by calling
 * {@link OH_Drawing_PenCreate}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleGetBackgroundPen(OH_Drawing_TextStyle*, OH_Drawing_Pen*);

/**
 * @brief Creates an <b>OH_Drawing_TypographyCreate</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param OH_Drawing_FontCollection Pointer to an <b>OH_Drawing_FontCollection</b> object, which is obtained by
 * calling {@link OH_Drawing_CreateFontCollection}.
 * @return Returns the pointer to the <b>OH_Drawing_TypographyCreate</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_TypographyCreate* OH_Drawing_CreateTypographyHandler(OH_Drawing_TypographyStyle*,
    OH_Drawing_FontCollection*);

/**
 * @brief Destroys an <b>OH_Drawing_TypographyCreate</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypographyHandler(OH_Drawing_TypographyCreate*);

/**
 * @brief Sets the text style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPushTextStyle(OH_Drawing_TypographyCreate*, OH_Drawing_TextStyle*);

/**
 * @brief Sets the text content.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @param char Pointer to the text content.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddText(OH_Drawing_TypographyCreate*, const char*);

/**
 * @brief Removes the topmost style in the stack, leaving the remaining styles in effect.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerPopTextStyle(OH_Drawing_TypographyCreate*);

/**
 * @brief Creates an <b>OH_Drawing_Typography</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @return Returns the pointer to the <b>OH_Drawing_Typography</b> object created.
 * @since 8
 * @version 1.0
 */
OH_Drawing_Typography* OH_Drawing_CreateTypography(OH_Drawing_TypographyCreate*);

/**
 * @brief Destroys an <b>OH_Drawing_Typography</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_DestroyTypography(OH_Drawing_Typography*);

/**
 * @brief Lays out the typography.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param double Maximum text width.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyLayout(OH_Drawing_Typography*, double /* maxWidth */);

/**
 * @brief Paints text on the canvas.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param OH_Drawing_Canvas Pointer to an <b>OH_Drawing_Canvas</b> object, which is obtained by calling
 * {@link OH_Drawing_CanvasCreate()}.
 * @param double X coordinate.
 * @param double Y coordinate.
 * @since 8
 * @version 1.0
 */
void OH_Drawing_TypographyPaint(OH_Drawing_Typography*, OH_Drawing_Canvas*,
    double /* potisionX */, double /* potisionY */);

/**
 * @brief Obtains the maximum width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the maximum width.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMaxWidth(OH_Drawing_Typography*);

/**
 * @brief Obtains the height.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the height.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetHeight(OH_Drawing_Typography*);

/**
 * @brief Obtains the width of the longest line. You are advised to round up the return value in actual use.
 * When the text content is empty, the minimum float value,
 * that is, -340282346638528859811704183484516925440.000000, is returned.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the width of the longest row.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetLongestLine(OH_Drawing_Typography*);

/**
 * @brief Obtains the minimum intrinsic width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the minimum intrinsic width.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMinIntrinsicWidth(OH_Drawing_Typography*);

/**
 * @brief Obtains the maximum intrinsic width.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the maximum intrinsic width.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetMaxIntrinsicWidth(OH_Drawing_Typography*);

/**
 * @brief Obtains the alphabetic baseline.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the alphabetic baseline.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetAlphabeticBaseline(OH_Drawing_Typography*);

/**
 * @brief Obtains the ideographic baseline.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the ideographic baseline.
 * @since 9
 * @version 1.1
 */
double OH_Drawing_TypographyGetIdeographicBaseline(OH_Drawing_Typography*);

/**
 * @brief Adds a placeholder.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyCreate Pointer to an <b>OH_Drawing_TypographyCreate</b> object. The pointer is obtained
 * by {@link OH_Drawing_CreateTypographyHandler}.
 * @param OH_Drawing_PlaceholderSpan Pointer to an <b>OH_Drawing_PlaceholderSpan</b> object.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_TypographyHandlerAddPlaceholder(OH_Drawing_TypographyCreate*, OH_Drawing_PlaceholderSpan*);

/**
 * @brief Checks whether the maximum number of lines is exceeded.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns <b>true</b> if the maximum number of lines is exceeded; returns <b>false</b> otherwise.
 * @since 11
 * @version 1.0
 */
bool OH_Drawing_TypographyDidExceedMaxLines(OH_Drawing_Typography*);

/**
 * @brief Obtains text boxes in a given range.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param size_t Start position.
 * @param size_t End position.
 * @param OH_Drawing_RectHeightStyle Height style. For details about the available options,
 * see {@link OH_Drawing_RectHeightStyle}.
 * @param OH_Drawing_RectWidthStyle Width style. For details about the available options,
 * see {@link OH_Drawing_RectWidthStyle}.
 * @return Returns the {@link OH_Drawing_TextBox} struct that holds the text boxes.
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBox* OH_Drawing_TypographyGetRectsForRange(OH_Drawing_Typography*,
    size_t, size_t, OH_Drawing_RectHeightStyle, OH_Drawing_RectWidthStyle);

/**
 * @brief Obtains text boxes for placeholders.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the {@link OH_Drawing_TextBox} struct that holds the text boxes.
 * @since 11
 * @version 1.0
 */
OH_Drawing_TextBox* OH_Drawing_TypographyGetRectsForPlaceholders(OH_Drawing_Typography*);

/**
 * @brief Obtains the left position of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by calling
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the left position.
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetLeftFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the right position of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by calling
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the right position.
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetRightFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the top position of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by calling
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the top position.
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetTopFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the bottom position of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by calling
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the bottom position.
 * @since 11
 * @version 1.0
 */
float OH_Drawing_GetBottomFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the text direction of a text box.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by calling
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @param int Index of the text box.
 * @return Returns the text direction.
 * @since 11
 * @version 1.0
 */
int OH_Drawing_GetTextDirectionFromTextBox(OH_Drawing_TextBox*, int);

/**
 * @brief Obtains the number of text boxes.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextBox Pointer to an <b>OH_Drawing_TextBox</b> object, which is obtained by calling
 * {@link OH_Drawing_TypographyGetRectsForRange} or {@link OH_Drawing_TypographyGetRectsForPlaceholders}.
 * @return Returns the number of text boxes.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetSizeOfTextBox(OH_Drawing_TextBox*);

/**
 * @brief Obtains the position and affinity of the glyph at the given coordinates.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param double X coordinate.
 * @param double Y coordinate.
 * @return Returns the {@link OH_Drawing_PositionAndAffinity} struct that holds the position and affinity of the glyph.
 * @since 11
 * @version 1.0
 */
OH_Drawing_PositionAndAffinity* OH_Drawing_TypographyGetGlyphPositionAtCoordinate(OH_Drawing_Typography*,
    double, double);

/**
 * @brief Obtains the position and affinity of the glyph cluster to which the glyph at the given coordinates belongs.
 * The glyph cluster is a container that holds one or more glyphs.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param double X coordinate.
 * @param double Y coordinate.
 * @return Returns the {@link OH_Drawing_PositionAndAffinity} struct that holds the position and affinity
 * of the glyph cluster.
 * @since 11
 * @version 1.0
 */
OH_Drawing_PositionAndAffinity* OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster(OH_Drawing_Typography*,
    double, double);

/**
 * @brief Obtains the position attribute of an <b>OH_Drawing_PositionAndAffinity</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PositionAndAffinity Pointer to an <b>OH_Drawing_PositionAndAffinity</b> object,
 * which is obtained by calling {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinate} or
 * {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster}.
 * @return Returns the position attribute.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetPositionFromPositionAndAffinity(OH_Drawing_PositionAndAffinity*);

/**
 * @brief Obtains the affinity attribute of an <b>OH_Drawing_PositionAndAffinity</b> object.
 * The affinity determines whether the font is close to the front text or rear text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_PositionAndAffinity Pointer to an <b>OH_Drawing_PositionAndAffinity</b> object,
 * which is obtained by calling {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinate} or
 * {@link OH_Drawing_TypographyGetGlyphPositionAtCoordinateWithCluster}.
 * @return Returns the affinity attribute.
 * @since 11
 * @version 1.0
 */
int OH_Drawing_GetAffinityFromPositionAndAffinity(OH_Drawing_PositionAndAffinity*);

/**
 * @brief Obtains the word boundary.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param size_t Index of the word.
 * @return Returns the {@link OH_Drawing_Range} struct that holds the word boundary.
 * @since 11
 * @version 1.0
 */
OH_Drawing_Range* OH_Drawing_TypographyGetWordBoundary(OH_Drawing_Typography*, size_t);

/**
 * @brief Obtains the start position of an <b>OH_Drawing_Range</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Range Pointer to an <b>OH_Drawing_Range</b> object, which is obtained by calling
 * {@link OH_Drawing_TypographyGetWordBoundary}.
 * @return Returns the start position.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetStartFromRange(OH_Drawing_Range*);

/**
 * @brief Obtains the end position of an <b>OH_Drawing_Range</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Range Pointer to an <b>OH_Drawing_Range</b> object, which is obtained by calling
 * {@link OH_Drawing_TypographyGetWordBoundary}.
 * @return Returns the end position.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_GetEndFromRange(OH_Drawing_Range*);

/**
 * @brief Obtains the number of lines.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the number of lines.
 * @since 11
 * @version 1.0
 */
size_t OH_Drawing_TypographyGetLineCount(OH_Drawing_Typography*);

/**
 * @brief Sets the text decoration style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Text decoration style. For details about the available options,
 * see {@link OH_Drawing_TextDecorationStyle}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationStyle(OH_Drawing_TextStyle*, int);

/**
 * @brief Sets the thickness scale factor of the text decoration line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Scale factor.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleDecorationThicknessScale(OH_Drawing_TextStyle*, double);

/**
 * @brief Sets the letter spacing for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Letter spacing.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleLetterSpacing(OH_Drawing_TextStyle*, double);

/**
 * @brief Sets the word spacing for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param double Word spacing.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleWordSpacing(OH_Drawing_TextStyle*, double);

/**
 * @brief Sets half leading for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param bool Whether to enable half leading. The value <b>true</b> means to enable half lading,
 * and <b>false</b> means the opposite.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleHalfLeading(OH_Drawing_TextStyle*, bool);

/**
 * @brief Sets the ellipsis content for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param char* Pointer to the ellipsis content. The data type is a pointer pointing to char.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleEllipsis(OH_Drawing_TextStyle*, const char*);

/**
 * @brief Sets the ellipsis style for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an <b>OH_Drawing_TextStyle</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Ellipsis style. For details about the available options, see {@link OH_Drawing_EllipsisModal}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTextStyleEllipsisModal(OH_Drawing_TextStyle*, int);

/**
 * @brief Sets the text break strategy.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Text break strategy. For details about the available options, see {@link OH_Drawing_BreakStrategy}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextBreakStrategy(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Sets the word break type.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Word break type. For details about the available options, see {@link OH_Drawing_WordBreakType}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextWordBreakType(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Sets the ellipsis style for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an <b>OH_Drawing_TypographyStyle</b> object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Ellipsis style. For details about the available options, see {@link OH_Drawing_EllipsisModal}.
 * @since 11
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextEllipsisModal(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Sets the ellipsis style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param char Pinter to an ellipsis style.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextEllipsis(OH_Drawing_TypographyStyle* style, const char* ellipsis);

/**
 * @brief Obtains the height of a line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param int Target line.
 * @return Returns the height.
 * @since 11
 * @version 1.0
 */
double OH_Drawing_TypographyGetLineHeight(OH_Drawing_Typography*, int);

/**
 * @brief Obtains the width of a line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an <b>OH_Drawing_Typography</b> object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param int Target line.
 * @return Returns the width.
 * @since 11
 * @version 1.0
 */
double OH_Drawing_TypographyGetLineWidth(OH_Drawing_Typography*, int);

/**
 * @brief Sets the text split ratio.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param float Text split ratio.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextSplitRatio(OH_Drawing_TypographyStyle* style, float textSplitRatio);

/**
 * @brief Checks whether the maximum number of lines is limited for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @return Returns <b>true</b> if that the maximum number of lines is limited; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyIsLineUnlimited(OH_Drawing_TypographyStyle* style);

/**
 * @brief Checks whether the text has an ellipsis.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @return Returns <b>true</b> if the text has an ellipsis; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyIsEllipsized(OH_Drawing_TypographyStyle* style);

/**
 * @brief Sets the text locale.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param char Pointer to the text locale.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLocale(OH_Drawing_TypographyStyle* style, const char* locale);

/**
 * @brief Obtains the font metrics.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an {@link OH_Drawing_Typography} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypography}.
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_Font_Metrics Pointer to an {@link OH_Drawing_Font_Metrics} object, which is obtained by
 * calling {@link OH_Drawing_Font_Metrics}.
 * @return Returns <b>true</b> if the font metrics is obtained; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TextStyleGetFontMetrics(OH_Drawing_Typography*, OH_Drawing_TextStyle*, OH_Drawing_Font_Metrics*);

/**
 * @brief Sets the text style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTextStyle}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextStyle(OH_Drawing_TypographyStyle*,OH_Drawing_TextStyle*);

/**
 * @brief Creates an <b>OH_Drawing_FontDescriptor</b> object to describe the detailed information about a system font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the {@link OH_Drawing_FontDescriptor} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontDescriptor* OH_Drawing_CreateFontDescriptor(void);

/**
 * @brief Destroys an <b>OH_Drawing_FontDescriptor</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontDescriptor Pointer to an {@link OH_Drawing_FontDescriptor} object, which is obtained by
 * calling {@link OH_Drawing_CreateFontDescriptor}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyFontDescriptor(OH_Drawing_FontDescriptor*);

/**
 * @brief Creates an <b>OH_Drawing_FontParser</b> object to parse a system font.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the {@link OH_Drawing_FontParser} object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontParser* OH_Drawing_CreateFontParser(void);

/**
 * @brief Destroys an <b>OH_Drawing_FontParser</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontParser Pointer to an {@link OH_Drawing_FontParser} object, which is obtained by calling
 * {@link OH_Drawing_CreateFontParser}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyFontParser(OH_Drawing_FontParser*);

/**
 * @brief Obtains the list of system fonts.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontParser Pointer to an {@link OH_Drawing_FontParser} object, which is obtained by calling
 * {@link OH_Drawing_CreateFontParser}.
 * @param size_t Pointer to the number of system font names.
 * @return Returns the system font list.
 * @since 12
 * @version 1.0
 */
char** OH_Drawing_FontParserGetSystemFontList(OH_Drawing_FontParser*, size_t*);

/**
 * @brief Reclaims the memory occupied by the system font list.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param char** Double pointer to the list of system font names.
 * @param size_t* Number of system font names.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroySystemFontList(char**, size_t);

/**
 * @brief Obtains information about a system font based on the font name.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_FontParser Pointer to an {@link OH_Drawing_FontParser} object, which is obtained by calling
 * {@link OH_Drawing_CreateFontParser}.
 * @param char* Pointer to the system font name.
 * @return Returns the system font.
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontDescriptor* OH_Drawing_FontParserGetFontByName(OH_Drawing_FontParser*, const char*);

/**
 * @brief Obtains the line metrics.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an {@link OH_Drawing_Typography} object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @return Returns the pointer to the {@link OH_Drawing_LineMetrics} object.
 * @since 12
 * @version 1.0
 */
OH_Drawing_LineMetrics* OH_Drawing_TypographyGetLineMetrics(OH_Drawing_Typography*);

/**
 * @brief Obtains the number of lines.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_LineMetrics Pointer to an {@link OH_Drawing_LineMetrics} object, which is obtained by calling
 * {@link OH_Drawing_LineMetrics}.
 * @return Returns the number of lines.
 * @since 12
 * @version 1.0
 */
size_t OH_Drawing_LineMetricsGetSize(OH_Drawing_LineMetrics*);

/**
 * @brief Destroys an <b>OH_Drawing_LineMetrics</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_LineMetrics Pointer to an {@link OH_Drawing_LineMetrics} object, which is obtained by calling
 * {@link OH_Drawing_LineMetrics}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyLineMetrics(OH_Drawing_LineMetrics*);

/**
 * @brief Obtains the metrics of a given line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an {@link OH_Drawing_Typography} object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param int Line No.
 * @param OH_Drawing_LineMetrics Pointer to an {@link OH_Drawing_LineMetrics} object, which is obtained by calling
 * {@link OH_Drawing_LineMetrics}.
 * @return Returns <b>true</b> if the metrics of the given line is obtained; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyGetLineMetricsAt(OH_Drawing_Typography*, int, OH_Drawing_LineMetrics*);

/**
 * @brief Obtains the metrics of a given line or the metrics of the first character in a given line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an {@link OH_Drawing_Typography} object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param int Line No.
 * @param bool Whether to obtain the metrics of the entire line. The value <b>true</b> means to obtain the metrics of
 * the entire line, and <b>false</b> means to obtain the metrics of the first character in the line.
 * @param bool Whether whitespace characters are included in the text width. The value <b>true</b> means that
 * whitespace characters are not included, <b>false</b> means the opposite.
 * @param OH_Drawing_LineMetrics Pointer to an {@link OH_Drawing_LineMetrics} object, which is obtained by calling
 * {@link OH_Drawing_LineMetrics}.
 * @return Returns <b>true</b> if the metrics of the given line or the metrics of the first character in the given line
 * is obtained; returns <b>false</b> otherwise.
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_TypographyGetLineInfo(OH_Drawing_Typography*, int, bool, bool, OH_Drawing_LineMetrics*);

/**
 * @brief Sets the font weight for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Font weight.
 * For details about the available options, see {@link OH_Drawing_FontWeight}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontWeight(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Sets the font style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Font style. For details about the available options, see {@link OH_Drawing_FontStyle}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontStyle(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Sets the font family name for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param char Pointer to the name of the font family.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontFamily(OH_Drawing_TypographyStyle*, const char*);

/**
 * @brief Sets the font size for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param double Font size, which must be greater than 0.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontSize(OH_Drawing_TypographyStyle*, double);

/**
 * @brief Sets the font height for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param double Font height.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextFontHeight(OH_Drawing_TypographyStyle*, double);

/**
 * @brief Sets whether half leading is used for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param bool Whether to enable half leading. The value <b>true</b> means to enable half lading,
 * and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextHalfLeading(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief Sets whether to enable the line style for text.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param bool Whether to enable the line style for text. The value <b>true</b> means to enable the line style for text,
 * and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextUseLineStyle(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief Sets the font weight of the text line style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Font weight.
 * For details about the available options, see {@link OH_Drawing_FontWeight}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontWeight(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Sets the font style of the text line style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Font style. For details about the available options, see {@link OH_Drawing_FontStyle}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontStyle(OH_Drawing_TypographyStyle*, int);

/**
 * @brief Sets the font families of the text line style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param int Number of font families.
 * @param char Pointer to the font families.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontFamilies(OH_Drawing_TypographyStyle*,
    int, const char* fontFamilies[]);

/**
 * @brief Sets the font size for a text line.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param double Font size, which must be greater than 0.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontSize(OH_Drawing_TypographyStyle*, double);

/**
 * @brief Sets the font height of the text line style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param double Font height.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleFontHeight(OH_Drawing_TypographyStyle*, double);

/**
 * @brief Sets whether half leading is used for the text line style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param bool Whether to enable half leading. The value <b>true</b> means to enable half lading,
 * and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleHalfLeading(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief Sets the spacing ratio of the text line style.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param double Spacing ratio.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleSpacingScale(OH_Drawing_TypographyStyle*, double);

/**
 * @brief Sets whether to enable the text line style only.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TypographyStyle Pointer to an {@link OH_Drawing_TypographyStyle} object, which is obtained by
 * calling {@link OH_Drawing_CreateTypographyStyle}.
 * @param bool Whether to enable the text line style only. The value <b>true</b> means to enable the text line style
 * only, and <b>false</b> means the opposite.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_SetTypographyTextLineStyleOnly(OH_Drawing_TypographyStyle*, bool);

/**
 * @brief Creates an <b>OH_Drawing_TextShadow</b> object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return Returns the pointer to the <b>OH_Drawing_TextShadow</b> object created.
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextShadow* OH_Drawing_CreateTextShadow(void);

/**
 * @brief Destroys an <b>OH_Drawing_TextShadow</b> object and reclaims the memory occupied by the object.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextShadow Pointer to an {@link OH_Drawing_TextShadow} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextShadow}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyTextShadow(OH_Drawing_TextShadow*);

/**
 * @brief Obtains a text shadow container.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @return Returns the pointer to the {@link OH_Drawing_TextShadow} object.
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextShadow* OH_Drawing_TextStyleGetShadows(OH_Drawing_TextStyle*);

/**
 * @brief Obtains the size of a text shadow container.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @return Returns the size.
 * @since 12
 * @version 1.0
 */
int OH_Drawing_TextStyleGetShadowCount(OH_Drawing_TextStyle*);

/**
 * @brief Adds a shadow to a text shadow container.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param OH_Drawing_TextShadow Pointer to an {@link OH_Drawing_TextShadow} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextShadow}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleAddShadow(OH_Drawing_TextStyle*, const OH_Drawing_TextShadow*);

/**
 * @brief Clears all shadows in a text shadow container.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TextStyleClearShadows(OH_Drawing_TextStyle*);

/**
 * @brief Obtains a shadow with a given index in a text shadow container.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextStyle Pointer to an {@link OH_Drawing_TextStyle} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextStyle}.
 * @param int Index.
 * @return Returns the pointer to the {@link OH_Drawing_TextShadow} object.
 * @since 12
 * @version 1.0
 */
OH_Drawing_TextShadow* OH_Drawing_TextStyleGetShadowWithIndex(OH_Drawing_TextStyle*, int);

/**
 * @brief Sets the indents for typography.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an {@link OH_Drawing_Typography} object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param int Number of indents.
 * @param float Array holding the indents.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_TypographySetIndents(OH_Drawing_Typography*, int, const float indents[]);

/**
 * @brief Obtains indents with a given index.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an {@link OH_Drawing_Typography} object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param int Index.
 * @return Returns the indents.
 * @since 12
 * @version 1.0
 */
float OH_Drawing_TypographyGetIndentsWithIndex(OH_Drawing_Typography*, int);

/**
 * @brief Obtains the line bounds.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Typography Pointer to an {@link OH_Drawing_Typography} object, which is obtained by calling
 * {@link OH_Drawing_CreateTypography}.
 * @param int Row index.
 * @param bool Whether the returned bounds contain spaces. The value <b>true</b> means that the bounds contain spaces,
 * and <b>false</b> means the opposite.
 * @return Returns the pointer to the {@link OH_Drawing_Range} object.
 * @since 12
 * @version 1.0
 */
OH_Drawing_Range* OH_Drawing_TypographyGetLineTextRange(OH_Drawing_Typography*, int, bool);

/**
 * @brief Reclaims the memory occupied by the vector consisting of the <b>OH_Drawing_TextShadow</b> objects.
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_TextShadow Pointer to an {@link OH_Drawing_TextShadow} object, which is obtained by calling
 * {@link OH_Drawing_CreateTextShadow}.
 * @since 12
 * @version 1.0
 */
void OH_Drawing_DestroyTextShadows(OH_Drawing_TextShadow*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
