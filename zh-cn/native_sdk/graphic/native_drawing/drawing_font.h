/*
 * Copyright (c) 2023-2024 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef C_INCLUDE_DRAWING_FONT_H
#define C_INCLUDE_DRAWING_FONT_H

/**
 * @addtogroup Drawing
 * @{
 *
 * @brief Drawing模块提供包括2D图形渲染、文字绘制和图片显示等功能函数。
 * 本模块采用屏幕物理像素单位px。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 *
 * @since 8
 * @version 1.0
 */

/**
 * @file drawing_font.h
 *
 * @brief 文件中定义了与字体相关的功能函数。
 *
 * 引用文件"native_drawing/drawing_font.h"
 * @library libnative_drawing.so
 * @since 11
 * @version 1.0
 */

#include "drawing_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief 字形边缘效果类型枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_FontEdging {
    /** 无抗锯齿处理。 */
    FONT_EDGING_ALIAS,
    /** 使用抗锯齿来平滑字形边缘。 */
    FONT_EDGING_ANTI_ALIAS,
    /** 使用次像素级别的抗锯齿来平滑字形边缘，可以获得更加平滑的字形渲染效果。 */
    FONT_EDGING_SUBPIXEL_ANTI_ALIAS,
} OH_Drawing_FontEdging;

/**
 * @brief 字形轮廓效果类型枚举。
 *
 * @since 12
 * @version 1.0
 */
typedef enum OH_Drawing_FontHinting {
    /** 不修改字形轮廓。 */
    FONT_HINTING_NONE,
    /** 最小限度修改字形轮廓以改善对比度。 */
    FONT_HINTING_SLIGHT,
    /** 修改字形轮廓以提高对比度。 */
    FONT_HINTING_NORMAL,
    /** 修改字形轮廓以获得最大对比度。 */
    FONT_HINTING_FULL,
} OH_Drawing_FontHinting;

/**
 * @brief 当前画布矩阵轴对齐时，将字形基线设置为是否与像素对齐。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @param baselineSnap 指示字形基线是否和像素对齐，true表示对齐，false表示不对齐。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetBaselineSnap(OH_Drawing_Font*, bool baselineSnap);

/**
 * @brief 当前画布矩阵轴对齐时，获取字形基线是否与像素对齐。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回字形基线是否与像素对齐，true为对齐，false为没有对齐。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsBaselineSnap(const OH_Drawing_Font*);

/**
 * @brief 用于设置字形边缘效果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @param OH_Drawing_FontEdging 字形边缘效果。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetEdging(OH_Drawing_Font*, OH_Drawing_FontEdging);

/**
 * @brief 获取字形边缘效果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回字形边缘效果。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontEdging OH_Drawing_FontGetEdging(const OH_Drawing_Font*);

/**
 * @brief 用于设置是否自动调整字形轮廓。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @param isForceAutoHinting 是否自动调整字形轮廓，true为自动调整，false为不自动调整。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetForceAutoHinting(OH_Drawing_Font*, bool isForceAutoHinting);

/**
 * @brief 获取字形轮廓是否自动调整。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回字形轮廓是否自动调整，true为自动调整，false为不自动调整。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsForceAutoHinting(const OH_Drawing_Font*);

/**
 * @brief 设置字形是否使用次像素渲染。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @param isSubpixel 字形是否使用次像素渲染，true为使用，false为不使用。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetSubpixel(OH_Drawing_Font*, bool isSubpixel);

/**
 * @brief 获取字形是否使用次像素渲染。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回字形是否使用次像素渲染，true为使用，false为不使用。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsSubpixel(const OH_Drawing_Font*);

/**
 * @brief 用于创建一个字体对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @return 函数会返回一个指针，指针指向创建的字体对象。
 * @since 11
 * @version 1.0
 */
OH_Drawing_Font* OH_Drawing_FontCreate(void);

/**
 * @brief 用于给字体设置字形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param OH_Drawing_Typeface 指向字形对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTypeface(OH_Drawing_Font*, OH_Drawing_Typeface*);

/**
 * @brief 获取字形对象。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象{@link OH_Drawing_Font}的指针。
 * @return OH_Drawing_Typeface 函数返回一个指针，指向字形对象{@link OH_Drawing_Typeface}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_Typeface* OH_Drawing_FontGetTypeface(OH_Drawing_Font*);

/**
 * @brief 用于给字体设置文字大小。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param textSize 字体大小。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSize(OH_Drawing_Font*, float textSize);

/**
 * @brief 获取字形文本大小。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回一个浮点数，表示字形文本大小。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetTextSize(const OH_Drawing_Font*);

/**
 * @brief 获取文本所表示的字符数量。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象{@link OH_Drawing_Font}的指针。
 * @param text 文本存储首地址。
 * @param byteLength 文本长度，单位为字节。
 * @param encoding 文本编码类型{@link OH_Drawing_TextEncoding}。
 * @since 12
 * @version 1.0
 */
int OH_Drawing_FontCountText(OH_Drawing_Font*, const void* text, size_t byteLength,
    OH_Drawing_TextEncoding encoding);

/**
 * @brief 用于将文本转换为字形索引。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @param text 文本存储首地址。
 * @param byteLength 文本长度，单位为字节。
 * @param encoding 文本编码类型{@link OH_Drawing_TextEncoding}。
 * @param glyphs 字形索引存储首地址，用于存储得到的字形索引。
 * @param maxGlyphCount 文本所表示的最大字符数量。
 * @return 返回字形索引数量。
 * @since 12
 * @version 1.0
 */
uint32_t OH_Drawing_FontTextToGlyphs(const OH_Drawing_Font*, const void* text, uint32_t byteLength,
    OH_Drawing_TextEncoding encoding, uint16_t* glyphs, int maxGlyphCount);

/**
 * @brief 用于获取字符串中每个字符的宽度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @param glyphs 字形索引存储首地址。
 * @param count 字形索引的数量。
 * @param widths 字形宽度存储首地址，用于存储得到的字形宽度。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontGetWidths(const OH_Drawing_Font*, const uint16_t* glyphs, int count, float* widths);

/**
 * @brief 用于设置线性可缩放字体。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param isLinearText 真为使能线性可缩放字体，假为不使能。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetLinearText(OH_Drawing_Font*, bool isLinearText);

/**
 * @brief 获取字形是否使用线性缩放。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回字形是否使用线性缩放，true为使用，false为不使用。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsLinearText(const OH_Drawing_Font*);

/**
 * @brief 用于给字体设置文本倾斜。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param skewX X轴相对于Y轴的倾斜度。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetTextSkewX(OH_Drawing_Font*, float skewX);

/**
 * @brief 获取字形文本在x轴上的倾斜度。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回一个浮点数，表示x轴上的文本倾斜度。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetTextSkewX(const OH_Drawing_Font*);

/**
 * @brief 用于设置增加描边宽度以近似粗体字体效果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @param isFakeBoldText 真为使能增加描边宽度，假为不使能。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontSetFakeBoldText(OH_Drawing_Font*, bool isFakeBoldText);

/**
 * @brief 获取是否增加笔画宽度以接近粗体字形。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回是否增加笔画宽度以接近粗体字形，true为增加，false为不增加。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsFakeBoldText(const OH_Drawing_Font*);

/**
 * @brief 用于设置字形对象在x轴上的缩放比例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @param scaleX 文本在x轴上的缩放比例。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetScaleX(OH_Drawing_Font*, float scaleX);

/**
 * @brief 获取字形对象在x轴上的缩放比例。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回文本在x轴上的缩放比例。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetScaleX(const OH_Drawing_Font*);

/**
 * @brief 用于设置字形轮廓效果。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @param OH_Drawing_FontHinting 字形轮廓枚举类型{@link OH_Drawing_FontHinting}。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetHinting(OH_Drawing_Font*, OH_Drawing_FontHinting);

/**
 * @brief 获取字形轮廓效果枚举类型。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return OH_Drawing_FontHinting 返回字形轮廓效果枚举类型{@link OH_Drawing_FontHinting}。
 * @since 12
 * @version 1.0
 */
OH_Drawing_FontHinting OH_Drawing_FontGetHinting(const OH_Drawing_Font*);

/**
 * @brief 用于设置字形是否转换成位图处理。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @param isEmbeddedBitmaps 设置字形是否转换成位图处理，true表示转换成位图处理，false表示不转换成位图处理。
 * @since 12
 * @version 1.0
 */
void OH_Drawing_FontSetEmbeddedBitmaps(OH_Drawing_Font*, bool isEmbeddedBitmaps);

/**
 * @brief 获取字形是否转换成位图处理。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字形对象{@link OH_Drawing_Font}的指针。
 * @return 返回字形是否转换成位图处理，true表示转换成位图处理，false表示不转换成位图处理。
 * @since 12
 * @version 1.0
 */
bool OH_Drawing_FontIsEmbeddedBitmaps(const OH_Drawing_Font*);

/**
 * @brief 用于销毁字体对象并回收该对象占有的内存。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象的指针。
 * @since 11
 * @version 1.0
 */
void OH_Drawing_FontDestroy(OH_Drawing_Font*);

/**
 * @brief 定义字体度量信息的结构体。
 *
 * @since 12
 * @version 1.0
 */
typedef struct OH_Drawing_Font_Metrics {
    /** 指示哪些度量是有效的 */
    uint32_t fFlags;
    /** 字符最高点到基线的最大距离 */
    float top;
    /** 字符最高点到基线的推荐距离 */
    float ascent;
    /** 字符最低点到基线的推荐距离 */
    float descent;
    /** 字符最低点到基线的最大距离 */
    float bottom;
    /** 行间距 */
    float leading;
    /** 平均字符宽度，如果未知则为零 */
    float avgCharWidth;
    /** 最大字符宽度，如果未知则为零 */
    float maxCharWidth;
    /** 任何字形边界框原点左侧的最大范围，通常为负值；不推荐使用可变字体 */
    float xMin;
    /** 任何字形边界框原点右侧的最大范围，通常为负值；不推荐使用可变字体 */
    float xMax;
    /** 小写字母的高度，如果未知则为零，通常为负数 */
    float xHeight;
    /** 大写字母的高度，如果未知则为零，通常为负数 */
    float capHeight;
    /** 下划线粗细 */
    float underlineThickness;
    /** 表示下划线的位置，即从基线到文字下方笔画顶部的垂直距离，通常为正值 */
    float underlinePosition;
    /** 删除线粗细 */
    float strikeoutThickness;
    /** 表示删除线的位置，即从基线到文字上方笔画底部的垂直距离，通常为负值 */
    float strikeoutPosition;
} OH_Drawing_Font_Metrics;

/**
 * @brief 获取字体度量信息。
 *
 * @syscap SystemCapability.Graphic.Graphic2D.NativeDrawing
 * @param OH_Drawing_Font 指向字体对象{@link OH_Drawing_Font}的指针。
 * @param OH_Drawing_Font_Metrics 指向字体度量信息对象{@link OH_Drawing_Font_Metrics}的指针。
 * @return 函数返回一个浮点数变量，表示建议的行间距。
 * @since 12
 * @version 1.0
 */
float OH_Drawing_FontGetMetrics(OH_Drawing_Font*, OH_Drawing_Font_Metrics*);

#ifdef __cplusplus
}
#endif
/** @} */
#endif
