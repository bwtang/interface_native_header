/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiGeofence
 * @{
 *
 * @brief 定义地理围栏的接口。
 *
 * 上层GNSS服务模块可以使用这个模块的接口来添加地理围栏，删除地理围栏，以及监视地理围栏状态的变化。
 *
 * @since 3.2
 */

/**
 * @file IGeofenceCallback.idl
 *
 * @brief 定义回调函数用于上报地理围栏服务是否可用、地理围栏事件、地理围栏操作结果等。
 *
 * 模块包路径：ohos.hdi.location.geofence.v1_0
 *
 * 引用：ohos.hdi.location.geofence.v1_0.GeofenceTypes
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.location.geofence.v1_0;

import ohos.hdi.location.geofence.v1_0.GeofenceTypes;

/**
 * @brief 定义回调函数用于上报地理围栏服务是否可用、地理围栏事件、地理围栏操作结果等。
 *
 * @since 3.2
 */
[callback] interface IGeofenceCallback {
    /**
     * @brief 上报地理围栏服务是否可用。
     *
     * @param isAvailable 表示地理围栏是否可用。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReportGeofenceAvailability([in] boolean isAvailable);

    /**
     * @brief 用于上报地理围栏事件。
     *
     * @param fenceIndex 表示地理围栏编号。
     * @param location 表示当前的位置，详情参考{@link Location}。
     * @param event 表示当前发生的地理围栏事件，详情参考{@link GeofenceEvent}。
     * @param timestamp 表示地理围栏事件发生的时刻。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReportGeofenceEvent([in] int fenceIndex,
                        [in] struct LocationInfo location,
                        [in] enum GeofenceEvent event,
                        [in] long timestamp);

    /**
     * @brief 上报围栏操作结果。
     *
     * @param fenceIndex 表示地理围栏编号。
     * @param type 表示地理围栏操作类型。详情参考{@link GeofenceOperateType}。
     * @param result 表示地理围栏操作结果，详情参考{@link GeofenceOperateResult}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    ReportGeofenceOperateResult([in] int fenceIndex,
                            [in] enum GeofenceOperateType type,
                            [in] enum GeofenceOperateResult result);
}
/** @} */