/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiAGnss
 * @{
 *
 * @brief 定义AGNSS模块的接口。
 *
 * 上层可以使用该模块提供的接口设置AGNSS回调、AGNSS服务器地址、AGNSS参考信息、setId等。
 *
 * @since 3.2
 */

/**
 * @file IAGnssCallback.idl
 *
 * @brief 定义AGNSS回调，用于请求上层建立或释放数据业务连接、请求上层下发setId、请求上层下发AGNSS参考信息。
 *
 * 模块包路径：ohos.hdi.location.agnss.v1_0
 *
 * 引用：ohos.hdi.location.agnss.v1_0.AGnssTypes
 *
 * @since 3.2
 * @version 1.0
 */

package ohos.hdi.location.agnss.v1_0;

import ohos.hdi.location.agnss.v1_0.AGnssTypes;

/**
 * @brief 定义AGNSS回调，用于请求上层建立或释放数据业务连接、请求上层下发setId、请求上层下发AGNSS参考信息。
 *
 * @since 3.2
 */
[callback] interface IAGnssCallback {
    /**
     * @brief 该接口用于请求上层建立或者释放数据业务连接。
     *
     * @param request 表示请求参数信息，详情参考{@link AGnssDataLinkRequest}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    RequestSetUpAgnssDataLink([in] struct AGnssDataLinkRequest request);

    /**
     * @brief 请求上层注入setId。
     *
     * @param type 表示setId类型，详情参考{@link SubscriberSetIdType}。
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    RequestSubscriberSetId([in] enum SubscriberSetIdType type);

    /**
     * @brief 请求上层注入AGNSS参考信息。
     *
     * @return 返回0表示成功，返回负数表示失败。
     *
     * @since 3.2
     * @version 1.0
     */
    RequestAgnssRefInfo();
}
/** @} */