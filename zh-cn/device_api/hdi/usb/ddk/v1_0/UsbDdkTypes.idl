/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @addtogroup HdiUsbDdk
 * @{
 *
 * @brief 提供USB DDK类型，并声明USB DDK API所需的宏、枚举变量和数据结构。
 *
 * @since 4.1
 * @version 1.0
 */

/**
 * @file UsbDdkTypes.idl
 *
 * @brief 提供USB DDK API中使用的枚举变量、结构和宏。
 *
 * 模块包路径：ohos.hdi.usb.ddk.v1_0
 *
 * @since 4.0
 * @version 1.0
 */

package ohos.hdi.usb.ddk.v1_0;

/**
 * @brief 控制传输的设置数据。它对应于USB协议中的<b>Setup Data</b>。
 *
 * @since 4.0
 * @version 1.0
 */
struct UsbControlRequestSetup {
     /** 请求类型，对应于USB协议中的bmRequestType。 */
    unsigned char requestType;
    /** 请求命令，对应USB协议中的bRequest。 */
    unsigned char requestCmd;
    /** 与USB协议中的wValue相对应的值。其含义因要求而异。 */
    unsigned short value;
    /** 与USB协议中的wIndex相对应的索引。它通常用于传输索引或偏移量。它的含义根据请求而不同。 */
    unsigned short index;
    /** 数据长度，对应于USB协议中的wLength。如果传输数据，则此字段指示传输的字节数。 */
    unsigned short length;
};

/**
 * @brief 标准设备描述符，对应于USB协议中的<b>标准设备描述符</b>。
 *
 * @since 4.0
 * @version 1.0
 */
struct UsbDeviceDescriptor {
    /** 描述符的大小，以字节为单位。 */
    unsigned char bLength;
    /** 描述符类型。 */
    unsigned char bDescriptorType;
     /** USB协议版本号。 */
    unsigned short bcdUSB;
    /** USB-IF分配的设备类别代码。 */
    unsigned char bDeviceClass;
    /** USB-IF分配的设备子类代码。该值受bDeviceClass的值限制。 */
    unsigned char bDeviceSubClass;
    /** USB-IF分配的协议代码。该值受bDeviceClass和bDeviceSubClass的限制。 */
    unsigned char bDeviceProtocol;
    /** 终结点0的最大数据包大小。只有值8、16、32和64是有效的。 */
    unsigned char bMaxPacketSize0;
    /** USB-IF分配的供应商ID。 */
    unsigned short idVendor;
    /** 供应商分配的产品ID。 */
    unsigned short idProduct;
    /** 设备发布编号。 */
    unsigned short bcdDevice;
    /** 描述供应商的字符串描述符的索引。 */
    unsigned char iManufacturer;
    /** 描述产品的字符串描述符的索引。 */
    unsigned char iProduct;
    /** 描述设备SN的字符串描述符的索引。 */
    unsigned char iSerialNumber;
    /** 配置数量。 */
    unsigned char bNumConfigurations;
};


/**
 * @brief 请求管道。
 *
 * @since 4.0
 * @version 1.0
 */
struct UsbRequestPipe {
   /** 接口操作处理。 */
   unsigned long interfaceHandle;
   /** 超时持续时间，以毫秒为单位。 */
   unsigned int timeout;
   /** 端点地址。 */
   unsigned char endpoint;
};
/** @} */