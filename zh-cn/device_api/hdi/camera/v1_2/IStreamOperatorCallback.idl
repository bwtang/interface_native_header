/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file IStreamOperatorCallback.idl
 *
 * @brief {@link IStreamOperator}相关的回调，这些回调均由调用者实现。
 *
 * 模块包路径：ohos.hdi.camera.v1_2
 *
 * 引用：ohos.hdi.camera.v1_2.Types
 *
 * @since 4.1
 * @version 1.2
 */
 

package ohos.hdi.camera.v1_2;

import ohos.hdi.camera.v1_2.Types;

/**
 * @brief 定义Camera设备流回调操作。
 *
 * 对Camera设备执行流回调的抓捕，结束，错误捕获和帧捕获等操作。
 *
 * @since 4.1
 * @version 1.2
 */
[callback] interface IStreamOperatorCallback extends ohos.hdi.camera.v1_0.IStreamOperatorCallback {
    /**
     * @brief 在捕获开始时调用。
     *
     * @param captureId 回调对应的捕获请求ID。
     * @param infos 捕获开始消息的列表。
     *
     * @since 4.1
     * @version 1.2
     */
    OnCaptureStartedV1_2([in] int captureId, [in] struct CaptureStartedInfo[] infos);
}
/** @} */