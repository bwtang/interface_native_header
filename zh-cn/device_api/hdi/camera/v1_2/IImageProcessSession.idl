/*
 * Copyright (c) 2023 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 /**
 * @addtogroup Camera
 * @{
 *
 * @brief Camera模块接口定义。
 *
 * Camera模块涉及相机设备的操作、流的操作、离线流的操作和各种回调等。
 *
 * @since 4.1
 * @version 1.2
 */

/**
 * @file IImageProcessSession.idl
 *
 * @brief 声明用于图像处理会话的API。
 *
 * 模块包路径：ohos.hdi.camera.v1_2
 *
 * 引用：ohos.hdi.camera.v1_2.Types
 *
 * @since 4.1
 * @version 1.2
 */
 

package ohos.hdi.camera.v1_2;

import ohos.hdi.camera.v1_2.Types;

/**
 * @brief 图像处理会话进程。
 *
 * 获取Coucurrency、待处理图像，设置执行模式，处理流程图像，删除图像，执行会话中断，会话重启。
 *
 * @since 4.1
 * @version 1.2
 */
interface IImageProcessSession {
    /**
     * @brief 获取具有spacific后处理执行模式的流程会话的Coucurrency任务计数。
     *
     * @param mode 执行模式。
     * @param taskCount coucurrency任务计数。
     *
     * @since 4.1
     * @version 1.2
     */
    GetCoucurrency([in] enum ExecutionMode mode, [out] int taskCount);

    /**
     * @brief 获取未处理的挂起图像的ID。
     *
     * @param imageIds 待处理图像的 ID。
     *
     * @since 4.1
     * @version 1.2
     */
    GetPendingImages([out] List<String> imageIds);

    /**
     * @brief 设置处理后执行模式。
     *
     * @param mode 执行模式。
     *
     * @since 4.1
     * @version 1.2
     */
    SetExecutionMode([in] ExecutionMode mode);

    /**
     * @brief 按镜像ID处理特定镜像。
     *
     * @param imageId 图像ID。
     *
     * @since 4.1
     * @version 1.2
     */
    ProcessImage([in] String imageId);

    /**
     * @brief 按映像ID删除特定映像。
     *
     * @param imageId 图像ID。
     *
     * @since 4.1
     * @version 1.2
     */
    RemoveImage([in] String imageId);

    /**
     * @brief 中断进程会话。
     *
     * @since 4.1
     * @version 1.2
     */
    Interrupt();

    /**
     * @brief 重置进程会话。
     *
     * @since 4.1
     * @version 1.2
     */
    Reset();
}
/** @} */